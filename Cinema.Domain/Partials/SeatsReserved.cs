﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Models
{
    [MetadataType(typeof(SeatsReservedMetadata))]    
    public partial class SeatsReserveds
    {
        internal class SeatsReservedMetadata
        {
            [ForeignKey("SeatId")]
            public virtual Seats Seats { get; set; }
            //[ForeignKey("BookingOptionsSet")]
           // public int BookingOptionsId { get; set; }
           
        }
    }
}
