﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Website.Controllers
{

    public class PayChoiceController : Controller
    {
        private cinemaEntities1 db = new cinemaEntities1();

        [HttpPost]
        // GET: PayChoice
        public ActionResult Pay(string payMethod)
        {
            //Gemaakt door: Arie Visser
            //Getest door: Merel Huijben

            ViewBag.Title = "Bestelling afdrukken";
            ViewBag.Header = "Bestelling afdrukken";

            
                //maak tickets aan PDF
                Bookings bookings = new Bookings();

                //Add booking options to db
                bookings.Payment = payMethod;
                bookings.Reserved = 1;
                bookings.MoviesShowingId = (int)Session["MovieShowingsID"];

                //Add ticket options to db
                var ticketOptions = (Models.TicketsSelectedModel)Session["ChosenTickets"];

                for (int i = 0; i < ticketOptions.ChosenTickets.Count; i++)
                {
                    if (ticketOptions.ChosenTickets[i].TicketOptionTypeId > 0)
                    {


                        bookings.BookingsBookingOptionsSets.Add(new BookingsBookingOptionsSets
                        {
                            BookingOptionsSet_Id = ticketOptions.ChosenTickets[i].TicketOptionTypeId,
                            total = ticketOptions.ChosenTickets[i].TotalOptionOrdered,
                        });
                    }
                }

                Random randomNumber = new Random();
                int generatedNo = randomNumber.Next(11111111, 99999999);

                bookings.UniqueId = generatedNo;
                Session["generatedNo"] = generatedNo;

                var chosenTickets = (Models.TicketsSelectedModel)Session["ChosenTickets"];

                var movieShowing = db.MoviesShowings.First(mo => mo.Id == bookings.MoviesShowingId);
                var allSeatIds = movieShowing.Rooms.Seats.Select(s => s.Id);
                var reservedSeatIds = movieShowing.Bookings.SelectMany(s => s.SeatsReserveds).Select(s => s.SeatId);
                var availableSeats = allSeatIds.Where(s => !reservedSeatIds.Contains(s));
                var totalChosenTickts = chosenTickets.ChosenTickets.Sum(ct => ct.TotalOrdered);

                if (availableSeats.Count() > totalChosenTickts)
                {
                    var ticketIdsToSave = availableSeats.Take(totalChosenTickts).ToList();
                    var ticketIdsToSaveCounter = 0;

                    for (int i = 0; i < chosenTickets.ChosenTickets.Count; i++)
                    {
                        for (int x = 0; x < chosenTickets.ChosenTickets[i].TotalOrdered; x++)
                        {
                            var seatsReserveds = new SeatsReserveds();
                            seatsReserveds.SeatId = ticketIdsToSave[ticketIdsToSaveCounter];
                            seatsReserveds.BookingTypesId = chosenTickets.ChosenTickets[i].TicketTypeId;
                            bookings.SeatsReserveds.Add(seatsReserveds);

                            ticketIdsToSaveCounter++;
                        }
                    }

                    db.Bookings.Add(bookings);
                    db.SaveChanges();
                }
                else
                {
                    // onvoldoende plek
                }

            
            //return View();

            return RedirectToAction("BookingSuccess", "Home", new { UniqueId = Session["generatedNo"]});
        }
    }
}