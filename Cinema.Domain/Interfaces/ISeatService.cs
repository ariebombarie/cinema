﻿using System;
using System.Collections.Generic;
using Domain.Models;

namespace Domain.Interfaces
{
    public interface ISeatService
    {
        List<Seats> GetSeats();

        void Add(Seats seats);

        List<Rooms> GetRooms();

      
    }
}
