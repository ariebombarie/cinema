﻿using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Domain.Models;
using System;
using Cinema.WebUI.Models;
using System.Net;
using System.Collections.Generic;
using iTextSharp.text.pdf;
using System.IO;
using iTextSharp.text;

namespace Cinema.Domain.Controllers
{
    public class HomeController : Controller
    {

        private cinemaEntities1 db = new cinemaEntities1();

        public ActionResult Index()
        {
            Session.Clear();
            return View();
        }

        public ActionResult Reservering()
        {
            //Gemaakt door: Arie Visser
            //Getest door: Merel Huijben
            ViewBag.Title = "Reservering";
            ViewBag.Header = "Reservering ophalen";
            return View();
        }

        public ActionResult Movie()
        {
            //Gemaakt door: Arie Visser
            //Getest door: Merel Huijben

            ViewBag.Title = "Film kiezen";
            ViewBag.Header = "Kies de gewenste film";
            var movies = db.MoviesShowings.Include(e => e.Movies).Include(e => e.Rooms);
            return View(movies.ToList());
        }

        public ActionResult Ticket(Movies model, MoviesShowings modelShowings)
        {
            //Gemaakt door: Arie Visser
            //Getest door: Merel Huijben

            Session["Moviename"] = model.Name;
            Session["MovieShowingsID"] = modelShowings.Id;

            //Bookings bookings = new Bookings();
            var movieShowing = db.MoviesShowings.First(m => m.Id == model.Id);
            var allSeatIds = movieShowing.Rooms.Seats.Select(s => s.Id);
            var reservedSeatIds = movieShowing.Bookings.SelectMany(s => s.SeatsReserveds).Select(s => s.SeatId);
            var availableSeats = allSeatIds.Where(s => !reservedSeatIds.Contains(s)).Count();

            Session["AvailableSeats"] = availableSeats;

            ViewBag.Title = "Kies uw ticket(s)";
            ViewBag.Header = "Kies hier je ticket(s) voor: " + Session["Moviename"];

            //var tickets = db.BookingTypes.Include(i => i.SeatsReserveds);
            // var ticketOption = db.BookingOptionsSet.ToList();

            var tickets = db.BookingTypes.Include(i => i.SeatsReserveds);
            var ticketOption = db.BookingOptionsSets.ToList();

            var movies = db.Movies.First(m => m.Name == model.Name);
            var movieType = movies.MovieTypes.Type;
            if (movieType == "3D")
            {
                TempData["3D"] = movieType;
            }
            else {
                TempData["3D"] = "Geen 3D!";
            }
            var view = new TicketModel()
            {
                BookingTypes = tickets,
                BookingOptionsSets = ticketOption
            };
            return View(view);

            // return View(tickets.ToList());
        }


        public ActionResult TicketsSelected(int[] ticketKind, int[] totalTickets, int[] ticketOption, int[] totalOptionTickets)
        {

            //Gemaakt door: Arie Visser
            //Getest door: Merel Huijben

            var model = new TicketsSelectedModel();
            var total = 0m; // Met d geef je aan dat het om een double gaat en met m een decimal (m staat voor money);
            var totalOptions = 0m;

            for (int i = 0; i < ticketKind.Count(); i++)
            {
                var ticketId = ticketKind[i];
                var bookingType = db.BookingTypes.First(b => b.Id == ticketId);
                var totalOrderedTickets = totalTickets[i];

                if (totalOrderedTickets > 0)
                {
                    model.ChosenTickets.Add(new ChosenTickets
                    {
                        TicketName = bookingType.Name,
                        TotalPrice = bookingType.Price.GetValueOrDefault(0) * totalOrderedTickets,
                        TotalOrdered = totalOrderedTickets,
                        TicketTypeId = ticketId,
                    });

                    total += bookingType.Price.GetValueOrDefault(0) * totalTickets[i];
                }
            }
            for (int o = 0; o < ticketOption.Count(); o++)
            {
                var ticketOptionId = ticketOption[o];
                var bookingOption = db.BookingOptionsSets.First(b => b.Id == ticketOptionId);
                var totalOrderedOptions = totalOptionTickets[o];

                if (totalOrderedOptions > 0)
                {
                    model.ChosenTickets.Add(new ChosenTickets
                    {
                        TicketOptionName = bookingOption.Name,
                        TotalOptionPrice = bookingOption.price * totalOrderedOptions,
                        TotalOptionOrdered = totalOrderedOptions,
                        TicketOptionTypeId = ticketOptionId,

                    });
                    totalOptions += bookingOption.price * totalOptionTickets[o];
                }
            }
            model.TotalPrice = total;
            model.TotalOptionsPrice = totalOptions;

            ViewBag.Title = "Controle";
            ViewBag.Header = "Controleer uw bestelling!";
            Session["ChosenTickets"] = model;

            return View(model);

        }


        public ActionResult PinCode()
        {
            //Gemaakt door: Merel Huijben
            //Getest door: Arie Visser

            ViewBag.Title = "Pin";
            ViewBag.Header = "Pin";

            return View();
        }

        public ActionResult AddBooking()
        {
            //Gemaakt door: Merel Huijben
            //Getest door: Arie Visser

            ViewBag.Title = "Bestelling afdrukken";
            ViewBag.Header = "Bestelling afdrukken";

            if (Session["PIN"] != null) {

                return RedirectToAction("BookingSuccess", "Home", new { UniqueId = Session["MovieShowingsID"] });
            }

            Bookings bookings = new Bookings();

            //Add booking options to db
            bookings.Payment = "pin";
            bookings.Reserved = 1;
            bookings.MoviesShowingId = (int)Session["MovieShowingsID"];

            //Add ticket options to db
            var ticketOptions = (TicketsSelectedModel)Session["ChosenTickets"];

            for (int i = 0; i < ticketOptions.ChosenTickets.Count; i++)
            { //count 3, 1x normaal ticket en 2x de opties?
                // ja zit ook ff te denken. Ik had 1 ticket toch?ja en 2x popcorn en 2x3D
                // mss sessieprobleem... tussentijds natuurlijk paar keer f5 gedaan..

                if (ticketOptions.ChosenTickets[i].TicketOptionTypeId > 0)
                {
                    //db.BookingsBookingOptionsSets.Add(new BookingsBookingOptionsSets
                    //{
                    //    BookingOptionsSet_Id = ticketOptions.ChosenTickets[i].TicketOptionTypeId,
                    //    total = ticketOptions.ChosenTickets[i].TotalOptionOrdered,
                    //    Bookings = bookings,
                    //});

                    bookings.BookingsBookingOptionsSets.Add(new BookingsBookingOptionsSets
                    {
                        BookingOptionsSet_Id = ticketOptions.ChosenTickets[i].TicketOptionTypeId,
                        total = ticketOptions.ChosenTickets[i].TotalOptionOrdered,
                    });
                }
            }

            Random randomNumber = new Random();
            //Provide the min and max limit for the no
            int generatedNo = randomNumber.Next(11111111, 99999999);

            bookings.UniqueId = generatedNo;

            var chosenTickets = (TicketsSelectedModel)Session["ChosenTickets"];

            var movieShowing = db.MoviesShowings.First(m => m.Id == bookings.MoviesShowingId);
            var allSeatIds = movieShowing.Rooms.Seats.Select(s => s.Id);
            var reservedSeatIds = movieShowing.Bookings.SelectMany(s => s.SeatsReserveds).Select(s => s.SeatId);
            var availableSeats = allSeatIds.Where(s => !reservedSeatIds.Contains(s));

            var totalChosenTickts = chosenTickets.ChosenTickets.Sum(ct => ct.TotalOrdered);

            if (availableSeats.Count() > totalChosenTickts)
            {
                var ticketIdsToSave = availableSeats.Take(totalChosenTickts).ToList();
                var ticketIdsToSaveCounter = 0;

                for (int i = 0; i < chosenTickets.ChosenTickets.Count; i++)
                {
                    for (int x = 0; x < chosenTickets.ChosenTickets[i].TotalOrdered; x++)
                    {
                        var seatsReserveds = new SeatsReserveds();
                        seatsReserveds.SeatId = ticketIdsToSave[ticketIdsToSaveCounter];
                        seatsReserveds.BookingTypesId = chosenTickets.ChosenTickets[i].TicketTypeId;
                        //seatsReserveds.BookingOptionsId = 1;
                        bookings.SeatsReserveds.Add(seatsReserveds);

                        ticketIdsToSaveCounter++;
                    }
                }

                db.Bookings.Add(bookings);
                db.SaveChanges();
            }
            else
            {
                // onvoldoende plek
            }

            return RedirectToAction("BookingSuccess", "Home", new { UniqueId = generatedNo });
        }

        public ActionResult BookingSuccess(int UniqueId)
        {
            return View(UniqueId);
        }

        // GET: Reservation
        public ActionResult Print(int? UniqueId)
        {

            //Gemaakt door: Arie Visser
            //Getest door: Merel Huijben
            Session["MovieShowingsID"] = UniqueId;

            Bookings bookings = db.Bookings.Where(i => i.UniqueId == UniqueId).First();
            if (bookings.Payment == "CASH")
            {
                var cash = "PIN";
                Session["PIN"] = cash;

                return View("~/Views/Home/PinCode.cshtml");
            }

            if (UniqueId == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var ticket = db.SeatsReserveds.Include(i => i.Bookings)
                .Where(e => e.Bookings.UniqueId == UniqueId);

            

            if (ticket.Any())
            {
                
                

                ViewBag.Title = "Tickets printen";
                ViewBag.Header = "Uw tickets worden geprint!";

                //return View(model);
                return View(ticket.ToList());
            }
            ViewBag.Title = "Reservering niet gevonden!";
            Session["foutmelding"] = ("Reservering niet gevonden!");
            return View("~/Views/Shared/Error.cshtml");

        }



        public void PDFTicket(int? UniqueId)
        {
            //Gemaakt door: Arie Visser
            //Getest door: Merel Huijben

            if (UniqueId == 0)
            {
                // return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var ticket = db.SeatsReserveds.Include(i => i.Bookings).Include(i => i.Bookings.BookingsBookingOptionsSets)
               .Where(e => e.Bookings.UniqueId == UniqueId);

            var booking = db.Bookings.First(b => b.UniqueId == UniqueId);

            // Hier using aanmaken en pdf openen
            BaseFont f_cb = BaseFont.CreateFont("c:\\windows\\fonts\\calibrib.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            BaseFont f_cn = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

            try
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    Document document = new Document(PageSize.A6, 2, 2, 2, 2);
                    PdfWriter writer = PdfWriter.GetInstance(document, ms);
                    document.Open();
                    foreach (var reservedSeat in booking.SeatsReserveds)
                    {
                        // pages for reserved seats
                        document.NewPage();
                        PdfContentByte cb = writer.DirectContent;
                        cb.SetFontAndSize(f_cn, 14);
                        cb.BeginText();
                        cb.SetTextMatrix(0, 0); // Left, Top
                                                //cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Titel:", 20, 200, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, reservedSeat.Bookings.MoviesShowings.Movies.Name, 140, 250, 0);
                        cb.SetFontAndSize(f_cn, 12);
                        //cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Type:", 20, 180, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, reservedSeat.BookingTypes.Name, 140, 180, 0);
                        //cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Datum:", 20, 160, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, reservedSeat.Bookings.MoviesShowings.DateTime.ToString("dd/MM yyyy - HH:mm"), 140, 160, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Zaal:", 40, 100, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, reservedSeat.Seats.Rooms.RoomNumber.ToString(), 120, 100, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Rij:", 40, 80, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, reservedSeat.Seats.RowNumber.ToString(), 120, 80, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Stoel:", 40, 60, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, reservedSeat.Seats.SeatNumber.ToString(), 120, 60, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Order Nr.:", 40, 40, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, reservedSeat.Bookings.UniqueId.ToString(), 120, 40, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Prijs:", 40, 20, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, '€' + reservedSeat.BookingTypes.Price.ToString(), 120, 20, 0);

                        cb.SetFontAndSize(f_cn, 9);
                        cb.EndText();

                    }

                    foreach (var item in booking.BookingsBookingOptionsSets)
                    {
                        for (int i = 0; i < item.total; i++)
                        {
                            //pages voor opties
                            //item.BookingOptionsSets.Name // 3d bril
                            document.NewPage();
                            PdfContentByte cb = writer.DirectContent;
                            cb.SetFontAndSize(f_cn, 12);
                            cb.BeginText();
                            cb.SetTextMatrix(10, 10); // Left, Top
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Titel:", 20, 20, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, item.Bookings.MoviesShowings.Movies.Name, 100, 20, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Datum:", 20, 40, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, item.Bookings.MoviesShowings.DateTime.ToString("dd/MM - HH:mm"), 100, 40, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Type:", 20, 60, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, item.BookingOptionsSets.Name, 100, 60, 0);

                            cb.SetFontAndSize(f_cn, 9);
                            cb.EndText();
                        }
                    }

                    // hier using sluiten
                    document.Close();
                    writer.Close();
                    ms.Close();

                    System.Web.HttpContext.Current.Response.ContentType = "pdf/application";
                    System.Web.HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=Ticket.pdf");
                    System.Web.HttpContext.Current.Response.OutputStream.Write(ms.GetBuffer(), 0, ms.GetBuffer().Length);

                }
            }
            // var result = ticket.ToList();
            // if (ticket == null)
            // {
            //     return HttpNotFound();
            // }
            //BaseFont f_cb = BaseFont.CreateFont("c:\\windows\\fonts\\calibrib.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            // BaseFont f_cn = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

            //try
            //{
            //    using (MemoryStream ms = new MemoryStream())
            //    {
            //        Document document = new Document(PageSize.A6, 2, 2, 2, 2);
            //        PdfWriter writer = PdfWriter.GetInstance(document, ms);
            //        document.Open();
            //        foreach (var item in result.GroupBy(i => i.Bookings))
            //        {
            //            foreach (var booking2 in item)
            //            {
            //                document.NewPage();
            //                PdfContentByte cb = writer.DirectContent;
            //                cb.SetFontAndSize(f_cn, 14);
            //                cb.BeginText();
            //                cb.SetTextMatrix(10, 10); // Left, Top
            //                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Titel:", 20, 20, 0);
            //                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, booking2.Bookings.MoviesShowings.Movies.Name, 100, 20, 0);
            //                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Datum:", 20, 40, 0);
            //                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, booking2.Bookings.MoviesShowings.DateTime.ToString("dd/MM - HH:mm"), 100, 40, 0);
            //                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Zaal:", 20, 60, 0);
            //                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, booking2.Seats.Rooms.RoomNumber.ToString(), 100, 60, 0);
            //                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Rij:", 20, 80, 0);
            //                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, booking2.Seats.RowNumber.ToString(), 100, 80, 0);
            //                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Stoel:", 20, 100, 0);
            //                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, booking2.Seats.SeatNumber.ToString(), 100, 100, 0);
            //                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Type:", 20, 120, 0);
            //                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, booking2.BookingTypes.Name, 100, 120, 0);
            //                cb.SetFontAndSize(f_cn, 9);
            //                cb.EndText();
            //            }

            //            foreach (var option in result.Select(i => i.Bookings.BookingsBookingOptionsSets))
            //            {

            //                foreach (var optionName in option)
            //                {
            //                   //6 opties teveel :P
            //                    for (int i = 0; i < optionName.total; i++)
            //                    {

            //                        document.NewPage();
            //                        PdfContentByte cb = writer.DirectContent;
            //                        cb.SetFontAndSize(f_cn, 12);
            //                        cb.BeginText();
            //                        cb.SetTextMatrix(10, 10); // Left, Top
            //                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Titel:", 20, 20, 0);
            //                        //cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, optionName.Bookings.MoviesShowings.Movies.Name, 100, 20, 0);
            //                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Datum:", 20, 40, 0);
            //                        //cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, optionName.Bookings.MoviesShowings.DateTime.ToString("dd/MM - HH:mm"), 100, 40, 0);
            //                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Type:", 20, 60, 0);
            //                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, optionName.BookingOptionsSets.Name, 100, 60, 0);

            //                        cb.SetFontAndSize(f_cn, 9);
            //                        cb.EndText();
            //                    }
            //                }
            //            }
            //            document.Close();
            //            writer.Close();
            //            ms.Close();

            //            System.Web.HttpContext.Current.Response.ContentType = "pdf/application";
            //            System.Web.HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=Ticket.pdf");
            //            System.Web.HttpContext.Current.Response.OutputStream.Write(ms.GetBuffer(), 0, ms.GetBuffer().Length);

            //        }
            //    }
            //}

            catch (Exception rror)
            {
                var error = "Error!";
                error = rror.Message;
            }
            // return Redirect(HttpContext.Request.UrlReferrer.AbsoluteUri);
        }
    }
}


