﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Partials
{
    public partial class cinemaEntities1 : DbContext
    {
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Primary keys
            modelBuilder.Entity<Bookings>().HasKey(q => q.Id);
            modelBuilder.Entity<BookingOptionsSets>().HasKey(q => q.Id);
            modelBuilder.Entity<BookingsBookingOptionsSets>().HasKey(q =>
                new {
                    q.Bookings_Id,
                    q.BookingOptionsSet_Id
                });

            // Relationships
            modelBuilder.Entity<BookingsBookingOptionsSets>()
                .HasRequired(t => t.Bookings)
                .WithMany(t => t.BookingsBookingOptionsSets)
                .HasForeignKey(t => t.Bookings_Id);

            modelBuilder.Entity<BookingsBookingOptionsSets>()
                .HasRequired(t => t.BookingOptionsSets)
                .WithMany(t => t.BookingsBookingOptionsSets)
                .HasForeignKey(t => t.BookingOptionsSet_Id);
            /*
            // Primary keys
        builder.Entity<User>().HasKey(q => q.UserID);
        builder.Entity<Email>().HasKey(q => q.EmailID);
        builder.Entity<UserEmail>().HasKey(q => 
            new { 
                q.UserID, q.EmailID
            });

        // Relationships
        builder.Entity<UserEmail>()
            .HasRequired(t => t.Email)
            .WithMany(t => t.UserEmails)
            .HasForeignKey(t => t.EmailID)

        builder.Entity<UserEmail>()
            .HasRequired(t => t.User)
            .WithMany(t => t.UserEmails)
            .HasForeignKey(t => t.UserID)
            */
        }
    }
}
