﻿using Domain.Interfaces;
using Domain.Models;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Services
{
    public class MoviesDbService : IMovieService
    {
        private readonly cinemaEntities1 _context;

        public MoviesDbService()
        {
            _context = new cinemaEntities1();
        }

        public void Add(Movies movie)
        {
            _context.Movies.Add(movie);
        }

        public List<Movies> GetMovies()
        {
            return _context.Movies.ToList();
        }

        public List<MoviesShowings> GetMoviesShowings()
        {
            var moviesShowings = new List<MoviesShowings>();
            var groups = _context.MoviesShowings.GroupBy(m => m.MoviesId).ToList();
            foreach (var group in groups)
            {
                var movie = group.First();
                moviesShowings.Add(movie);
            }

            return moviesShowings;

        }
    }
}
