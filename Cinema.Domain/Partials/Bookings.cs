﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Models
{
    [MetadataType(typeof(BookingsMetadata))]
    public partial class Bookings
    {
        internal class BookingsMetadata
        {
            [ForeignKey("MoviesShowingId")]
            public virtual MoviesShowings MoviesShowings { get; set; }

            [ForeignKey("BookingOptionsSet_Id")]
            public virtual ICollection<BookingsBookingOptionsSets> BookingsBookingOptionsSets { get; set; }
        }
    }
}
