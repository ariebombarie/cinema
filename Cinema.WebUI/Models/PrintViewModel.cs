﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cinema.WebUI.Models
{
    public class PrintViewModel
    {
        public string MovieName { get; set; }
        public int Id { get; set; }
        public int Room { get; set; }
        public short Reserved { get; set; }
        public int SeatNumber { get; set; }
        public int RowNumber { get; set; }
        public DateTime Time { get; set; }
        public string TicketType { get; set; }
    }
}