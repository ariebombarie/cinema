﻿using System.Web.Mvc;
using Website.Models;

namespace Website.Controllers
{
    public class IMDBController : Controller
    {
        //Gemaakt door: Timo Hoogendorp
        //Getest door: Arie Visser
        [HttpPost]
        public ActionResult IMDBMovie(string MovieName)
        {
            string IMDBMovieName = MovieName;

            IMDb imdb = new IMDb(IMDBMovieName, true);
            string Title = imdb.Title;

            if (imdb.Title != null)
                return View(imdb);
            else
                Session["foutmelding"] = ("Deze film is niet gevonden!");
                return View("~/Views/Shared/Error.cshtml");
        }
            //rest volgt vanavond!

  
        }
    }