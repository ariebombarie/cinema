﻿using System;
using System.IO;
using Domain.Interfaces;
using Domain.Models;
using System.Collections.Generic;
using System.Linq;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace Domain.Services
{
    public class PDFTicketService : ITicketService
    {
        private cinemaEntities1 _context;

        public List<Bookings> GetBookings()
        {
            throw new NotImplementedException();
        }

        public void TicketPrint(int? UniqueId)
        {


            //Gemaakt door: Arie Visser
            //Getest door: Merel Huijben

            if (UniqueId == 0)
            {
                // return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            _context = new cinemaEntities1();
            var booking = _context.Bookings.First(b => b.UniqueId == UniqueId);

            if (booking.Payment == "CASH")
            {
                //maak reserveringsnummer aan PDF
                BaseFont f_cb = BaseFont.CreateFont("c:\\windows\\fonts\\calibrib.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                BaseFont f_cn = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

                try
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        Document document = new Document(PageSize.A6, 0, 0, 0, 0);
                        
                        PdfWriter writer = PdfWriter.GetInstance(document, ms);
                        document.Open();
                        foreach (var reservedSeat in booking.SeatsReserveds.Take(1))
                        {
                            // pages for reserved seats
                            document.NewPage();
                            PdfContentByte cb = writer.DirectContent;
                            cb.SetFontAndSize(f_cb, 14);
                            cb.BeginText();
                            cb.SetTextMatrix(0, 0); // Left, Top
                            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Order Nr.:", 140, 300, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, reservedSeat.Bookings.UniqueId.ToString(), 140, 250, 0);
                            cb.SetFontAndSize(f_cn, 12);
                            //cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Type:", 20, 180, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, reservedSeat.Bookings.MoviesShowings.Movies.Name, 140, 180, 0);
                            //cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Datum:", 20, 160, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, reservedSeat.Bookings.MoviesShowings.DateTime.ToString("dd/MM yyyy - HH:mm"), 140, 160, 0);

                            cb.SetFontAndSize(f_cn, 9);
                            cb.EndText();
                            iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance("http://www.c-sharpcorner.com/App_Themes/CSharp/Images/CSSiteLogo.gif");

                        }
                        // hier using sluiten
                        document.Close();
                        writer.Close();
                        ms.Close();

                        System.Web.HttpContext.Current.Response.ContentType = "pdf/application";
                        System.Web.HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=Ticket.pdf");
                        System.Web.HttpContext.Current.Response.OutputStream.Write(ms.GetBuffer(), 0, ms.GetBuffer().Length);

                    }
                }


                catch (Exception rror)
                {
                    var error = "Error!";
                    error = rror.Message;
                }
            }

            else
            {

                // Hier using aanmaken en pdf openen
                BaseFont f_cb = BaseFont.CreateFont("c:\\windows\\fonts\\calibrib.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                BaseFont f_cn = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

                try
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        Document document = new Document(PageSize.A6, 0, 0, 0, 0);
                        PdfWriter writer = PdfWriter.GetInstance(document, ms);
                        document.Open();
                        foreach (var reservedSeat in booking.SeatsReserveds)
                        {
                            // pages for reserved seats
                            document.NewPage();
                            PdfContentByte cb = writer.DirectContent;
                            cb.SetFontAndSize(f_cb, 14);
                            cb.BeginText();
                            cb.SetTextMatrix(0, 0); // Left, Top
                                                    //cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Titel:", 20, 200, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, reservedSeat.Bookings.MoviesShowings.Movies.Name, 140, 250, 0);
                            cb.SetFontAndSize(f_cn, 12);
                            //cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Type:", 20, 180, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, reservedSeat.BookingTypes.Name, 140, 180, 0);
                            //cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Datum:", 20, 160, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, reservedSeat.Bookings.MoviesShowings.DateTime.ToString("dd/MM yyyy - HH:mm"), 140, 160, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Zaal:", 40, 100, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, reservedSeat.Seats.Rooms.RoomNumber.ToString(), 120, 100, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Rij:", 40, 80, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, reservedSeat.Seats.RowNumber.ToString(), 120, 80, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Stoel:", 40, 60, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, reservedSeat.Seats.SeatNumber.ToString(), 120, 60, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Order Nr.:", 40, 40, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, reservedSeat.Bookings.UniqueId.ToString(), 120, 40, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Prijs:", 40, 20, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, '€' + reservedSeat.BookingTypes.Price.ToString(), 120, 20, 0);

                            cb.SetFontAndSize(f_cn, 9);
                            cb.EndText();
                            iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance("http://www.c-sharpcorner.com/App_Themes/CSharp/Images/CSSiteLogo.gif");

                        }

                        foreach (var item in booking.BookingsBookingOptionsSets)
                        {
                            for (int i = 0; i < item.total; i++)
                            {
                                //pages voor opties
                                //item.BookingOptionsSets.Name // 3d bril
                                document.NewPage();
                                PdfContentByte cb = writer.DirectContent;
                                cb.SetFontAndSize(f_cn, 12);
                                cb.BeginText();
                                cb.SetTextMatrix(10, 10); // Left, Top
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Titel:", 20, 20, 0);
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, item.Bookings.MoviesShowings.Movies.Name, 100, 20, 0);
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Datum:", 20, 40, 0);
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, item.Bookings.MoviesShowings.DateTime.ToString("dd/MM - HH:mm"), 100, 40, 0);
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Type:", 20, 60, 0);
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, item.BookingOptionsSets.Name, 100, 60, 0);

                                cb.SetFontAndSize(f_cn, 9);
                                cb.EndText();
                            }
                        }

                        // hier using sluiten
                        document.Close();
                        writer.Close();
                        ms.Close();

                        System.Web.HttpContext.Current.Response.ContentType = "pdf/application";
                        System.Web.HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=Ticket.pdf");
                        System.Web.HttpContext.Current.Response.OutputStream.Write(ms.GetBuffer(), 0, ms.GetBuffer().Length);

                    }
                }


                catch (Exception rror)
                {
                    var error = "Error!";
                    error = rror.Message;
                }

            }
        }
    }
}