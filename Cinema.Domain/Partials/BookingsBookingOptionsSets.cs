﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    [MetadataType(typeof(BookingsBookingOptionsSetsMetadata))]
    public partial class BookingsBookingOptionsSets
    {
        internal class BookingsBookingOptionsSetsMetadata
        {
            [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
            public int Id { get; set; }
        }
    }
}