﻿using Domain.Interfaces;
using Domain.Models;
using Website.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Website.Controllers
{
    public class MoviesController : Controller
    {

        private readonly IMovieService _movies;
        public MoviesController(IMovieService movies)
        {
            _movies = movies;
        }

        public ActionResult Movie()
        {
            //Gemaakt door: Arie Visser
            //Getest door: Merel Huijben
            var m = _movies.GetMovies();

            var model = from movie in m
                        select new IndexModel
                        {
                            Id = movie.Id,
                            ImageData = movie.ImageData,
                            Name = movie.Name,
                            Genres = movie.Genres,
                           // Info = movie.Movies.Info,
                            Plot = movie.Plot,
                            Duration = movie.Duration,
                            Trailer = movie.TrailerUrl,
                            AgeRating = movie.AgeRating,
                            //MovieShowingId = movie.Id
                            ReleaseDate = movie.ReleaseDate,
                           
                        };


            return View(model);
        }
        public ActionResult MovieSelected(int id, string movieName)
        {
            Session["movieName"] = movieName;
            //Gemaakt door: Arie Visser
            //Getest door: Timo Hoogendorp
            var m = _movies.GetMovies();

            var model = from movie in m
                        select new IndexModel
                        {
                            Id = movie.Id,
                            ImageData = movie.ImageData,
                            Name = movie.Name,
                          //  Info = movie.Movies.Info,
                            Genres = movie.Genres,
                            Plot = movie.Plot,
                            Duration = movie.Duration,
                            Trailer = movie.TrailerUrl,
                            AgeRating = movie.AgeRating,
                            MovieId = id,
                            ReleaseDate = movie.ReleaseDate,
                            MovieTypes = movie.MovieTypes

                        };


            return View(model);
        }
    }
}