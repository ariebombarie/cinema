﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Models;
using Domain.Interfaces;

namespace Cinema.Unittests.Mocks
{
    public class MoviesServiceMock : IMovieService
    {
        private List<Movies> _movies;
        private List<MoviesShowings> _moviesShowings;

        public MoviesServiceMock()
        {
            _movies = new List<Movies>();
            _moviesShowings = new List<MoviesShowings>();
        }

        public int GetMoviesShowingsCallCount { get; set; }

        public List<Movies> GetMovies()
        {
            return _movies;
        }

        public void Add(Movies movie)
        {
            _movies.Add(movie);
        }
        public void Add(MoviesShowings movie)
        {
            _moviesShowings.Add(movie);
        }

        public List<MoviesShowings> GetMoviesShowings()
        {
            GetMoviesShowingsCallCount++;
            return _moviesShowings;
        }
    }
}
