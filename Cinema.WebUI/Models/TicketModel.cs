﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cinema.WebUI.Models
{
    public class TicketModel
    {
        public TicketModel()
        {
        }

        public List<BookingOptionsSets> BookingOptionsSets { get; set; }
        public IQueryable<BookingTypes> BookingTypes { get; set; }

    }
    }
