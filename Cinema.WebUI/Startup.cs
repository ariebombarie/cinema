﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Cinema.Domain.Startup))]
namespace Cinema.Domain
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
