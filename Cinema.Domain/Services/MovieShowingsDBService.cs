﻿using Domain.Interfaces;
using Domain.Models;
using System.Collections.Generic;
using System.Linq;
using System;

namespace Domain.Services
{
    public class MovieShowingsDBService : IMovieShowingsService
    {
        private readonly cinemaEntities1 _context;

        public MovieShowingsDBService()
        {
            _context = new cinemaEntities1();
        }

        public void Add(MoviesShowings moviesShowings)
        {
            _context.MoviesShowings.Add(moviesShowings);
        }

        public List<MoviesShowings> GetMovieShowings()
        {
            return _context.MoviesShowings.ToList();
        }

        // public List<MoviesShowings> GetMovies()
        // {
        //    return _context.MoviesShowings.ToList();
        //}

        public List<MoviesShowings> GetMoviesShowings()
        {
            var moviesShowings = _context.MoviesShowings.ToList();
            //var moviesShowings = new List<MoviesShowings>();
            // var groups = _context.MoviesShowings.GroupBy(m => m.MoviesId).ToList();
            //foreach (var group in groups)
            // {
            //     var movie = group.First();
            //     moviesShowings.Add(movie);
            // }

            return moviesShowings;

        }
    }
}
