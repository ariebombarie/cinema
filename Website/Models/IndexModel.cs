﻿using Domain.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Website.Models
{

    public class IndexModel 
    {
        public int Id { get; set; }
        public int MovieId { get; set; }
        public string Name { get; set; }
        public string ImageData { get; set; }
        public int GenresId { get; set; }
        public Nullable<int> AgeRating { get; set; }
        public string Plot { get; set; }
        public Nullable<int> Duration { get; set; }
        public string Trailer { get; set; }
        public int MovieTypeId { get; set; }
        public int MovieShowingId { get; set; }
        public virtual MovieTypes MovieTypes { get; set; }
        public virtual Genres Genres { get; set; }
        public DateTime DateTime { get; set; }
        public Nullable<System.DateTime> ReleaseDate { get; set; }
        public virtual ICollection<MoviesShowings> MoviesShowings { get; set; }
        //public virtual Movies Movies { get; set; }
        public virtual Rooms Rooms { get; set; }
    }
}