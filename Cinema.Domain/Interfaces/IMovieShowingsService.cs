﻿using Domain.Models;
using System.Collections.Generic;

namespace Domain.Interfaces { 

    public interface IMovieShowingsService
{
    List<MoviesShowings> GetMovieShowings();

    void Add(MoviesShowings moviesShowings);

   // List<MoviesShowings> GetMoviesShowings();
}
}
