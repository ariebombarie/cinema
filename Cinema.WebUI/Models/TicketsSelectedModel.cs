﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cinema.WebUI.Models
{
    public class TicketsSelectedModel
    {
        public TicketsSelectedModel()
        {
            ChosenTickets = new List<Models.ChosenTickets>();
        }

        public decimal TotalPrice { get; set; }
        public decimal TotalOptionsPrice { get; set; }
        public List<ChosenTickets> ChosenTickets { get; set; }

    }

    public class ChosenTickets
    {
        public decimal TotalPrice { get; set; }
        public decimal TotalOptionPrice { get; set; }

        public int TotalOrdered { get; set; }
        public int TotalOptionOrdered { get; set; }
        

        public int TicketTypeId { get; set; }
        public int TicketOptionTypeId { get; set; }

        public string TicketName { get; set; }
        public string TicketOptionName { get; set; }
        
    }
}