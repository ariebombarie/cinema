﻿using System.Linq;
using System.Web.Mvc;
using Domain.Models;
using Website.Models;
using Domain.Interfaces;


namespace Website.Controllers
{
    public class HomeController : Controller
    {

        private cinemaEntities1 db = new cinemaEntities1();

        //Gemaakt door: Arie Visser
        //Getest door: Merel Huijben

        private IMovieService _movies;
        private ITicketService _ticket;

        public HomeController(IMovieService movies, ITicketService tickets)
        {
            _movies = movies;
            _ticket = tickets;
        }

        public HomeController(IMovieService movies)
        {
            _movies = movies;
        }

        public HomeController(ITicketService tickets)
        {
            _ticket = tickets;
        }

        public ActionResult Index()
        {
            //Gemaakt door: Arie Visser
            //Getest door: Merel Huijben
            var m = _movies.GetMoviesShowings();


            var model = from movie in m
                        select new IndexModel
                        {
                            Id = movie.Movies.Id,
                            ImageData = movie.Movies.ImageData,
                            Name = movie.Movies.Name,
                            Genres = movie.Movies.Genres,
                            Plot = movie.Movies.Plot,
                            Duration = movie.Movies.Duration,
                            Trailer = movie.Movies.TrailerUrl,
                            AgeRating = movie.Movies.AgeRating,
                            MovieShowingId = movie.Id
                        };

            return View(model);
        }


        public ActionResult Movie(int id)
        {
            //Gemaakt door: Arie Visser
            //Getest door: Merel Huijben

            var showings = db.MoviesShowings.Where(i => i.Id == id);
            return View(showings.ToList());
        }


        public void PDFTicket(int? UniqueId)
        {
            //Gemaakt door: Arie Visser
            //Getest door: Merel Huijben

            _ticket.TicketPrint(UniqueId);

        }
        public ActionResult Contact()
        {
            //Gemaakt door: Timo Hoogendorp
            //Getest door: Arie Visser

            return View();
        }
        public ActionResult BookingSuccess()
        {
            //Gemaakt door: Arie Visser
            //Getest door: Merel Huijben

            return View();
        }
        public ActionResult IndexBookingType() {


            //Gemaakt door: Merel Huijben
            //Getest door: Arie Visser

            return View();
        }
    }
}