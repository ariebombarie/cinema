﻿using Domain.Models;
using System.Collections.Generic;


namespace Domain.Interfaces
{
    public interface IMovieService
    {
        List<Movies> GetMovies();

        void Add(Movies movie);

        List<MoviesShowings> GetMoviesShowings();
    }
}
