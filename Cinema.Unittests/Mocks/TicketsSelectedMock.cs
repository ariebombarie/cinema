﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Models;
using Domain.Interfaces;


namespace Cinema.Unittests.Mocks
{
    class TicketsSelectedMock : ITicketService

    {
        private List<BookingTypes> _Bookingtypes;
        private List<BookingOptionsSets> _BookingOptionsSets;

        public TicketsSelectedMock()

        {
            _Bookingtypes = new List<BookingTypes>();
            _BookingOptionsSets = new List<BookingOptionsSets>();
        }

        public int TicketTypeTest { get; set; }

        public List<BookingTypes> GetBookingTypes()

        {
            return _Bookingtypes;
        }
        public void Add(BookingTypes BookingType)
        {
            _Bookingtypes.Add(BookingType);
        }
        public void Add(BookingOptionsSets movie)
        {
            _BookingOptionsSets.Add(movie);
        }

        public List<BookingOptionsSets> GetBookingOptionsSets()
        {
            TicketTypeTest++;
            return _BookingOptionsSets;
        }
        //let op! nog aanpassen
        public void TicketPrint(int? UniqueId)
        {
            throw new NotImplementedException();
        }
    }

}