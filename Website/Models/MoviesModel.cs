﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Website.Models
{
    public class Group<T, K>
    {
        public K Key;
        public IEnumerable<T> Values;
    }
}