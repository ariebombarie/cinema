//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Domain.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class BookingOptionsSetBookings
    {
        public int BookingOptionsSet_Id { get; set; }
        public int Bookings_Id { get; set; }
        public string Name { get; set; }
        public decimal price { get; set; }
    
        public virtual Bookings Bookings { get; set; }
    }
}
