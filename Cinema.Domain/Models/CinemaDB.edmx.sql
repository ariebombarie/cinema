
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 06/05/2016 20:35:03
-- Generated from EDMX file: C:\Users\Arie\Desktop\Cinema\Cinema.Domain\Models\CinemaDB.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [cinema];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_BookingOptionsSeatsReserved]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SeatsReserveds] DROP CONSTRAINT [FK_BookingOptionsSeatsReserved];
GO
IF OBJECT_ID(N'[dbo].[FK_FKBookings307328]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Bookings] DROP CONSTRAINT [FK_FKBookings307328];
GO
IF OBJECT_ID(N'[dbo].[FK_FKBookings613582]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Bookings] DROP CONSTRAINT [FK_FKBookings613582];
GO
IF OBJECT_ID(N'[dbo].[FK_FKEmployees288785]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Employees] DROP CONSTRAINT [FK_FKEmployees288785];
GO
IF OBJECT_ID(N'[dbo].[FK_FKMovies597736]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Movies] DROP CONSTRAINT [FK_FKMovies597736];
GO
IF OBJECT_ID(N'[dbo].[FK_FKMovies768699]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Movies] DROP CONSTRAINT [FK_FKMovies768699];
GO
IF OBJECT_ID(N'[dbo].[FK_FKMoviesShow559004]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MoviesShowings] DROP CONSTRAINT [FK_FKMoviesShow559004];
GO
IF OBJECT_ID(N'[dbo].[FK_FKMoviesShow710895]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MoviesShowings] DROP CONSTRAINT [FK_FKMoviesShow710895];
GO
IF OBJECT_ID(N'[dbo].[FK_FKRooms613752]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Rooms] DROP CONSTRAINT [FK_FKRooms613752];
GO
IF OBJECT_ID(N'[dbo].[FK_FKSeats497775]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Seats] DROP CONSTRAINT [FK_FKSeats497775];
GO
IF OBJECT_ID(N'[dbo].[FK_FKSeatsReser457498]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SeatsReserveds] DROP CONSTRAINT [FK_FKSeatsReser457498];
GO
IF OBJECT_ID(N'[dbo].[FK_FKSeatsReser829977]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SeatsReserveds] DROP CONSTRAINT [FK_FKSeatsReser829977];
GO
IF OBJECT_ID(N'[dbo].[FK_SeatsReservedsBookings]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SeatsReserveds] DROP CONSTRAINT [FK_SeatsReservedsBookings];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[BookingOptionsSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[BookingOptionsSet];
GO
IF OBJECT_ID(N'[dbo].[Bookings]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Bookings];
GO
IF OBJECT_ID(N'[dbo].[BookingTypes]', 'U') IS NOT NULL
    DROP TABLE [dbo].[BookingTypes];
GO
IF OBJECT_ID(N'[dbo].[Cinemas]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Cinemas];
GO
IF OBJECT_ID(N'[dbo].[Customers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Customers];
GO
IF OBJECT_ID(N'[dbo].[Employees]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Employees];
GO
IF OBJECT_ID(N'[dbo].[Genres]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Genres];
GO
IF OBJECT_ID(N'[dbo].[Movies]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Movies];
GO
IF OBJECT_ID(N'[dbo].[MoviesShowings]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MoviesShowings];
GO
IF OBJECT_ID(N'[dbo].[MovieTypes]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MovieTypes];
GO
IF OBJECT_ID(N'[dbo].[Rooms]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Rooms];
GO
IF OBJECT_ID(N'[dbo].[Seats]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Seats];
GO
IF OBJECT_ID(N'[dbo].[SeatsReserveds]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SeatsReserveds];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'BookingOptionsSet'
CREATE TABLE [dbo].[BookingOptionsSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [price] decimal(18,2)  NOT NULL
);
GO

-- Creating table 'Bookings'
CREATE TABLE [dbo].[Bookings] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Payment] nvarchar(max)  NOT NULL,
    [Reserved] smallint  NULL,
    [CustomersId] int  NULL,
    [MoviesShowingId] int  NOT NULL,
    [Customersemail] nvarchar(max)  NULL,
    [UniqueId] int  NOT NULL
);
GO

-- Creating table 'BookingTypes'
CREATE TABLE [dbo].[BookingTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Price] decimal(18,2)  NULL
);
GO

-- Creating table 'Cinemas'
CREATE TABLE [dbo].[Cinemas] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [City] nvarchar(max)  NOT NULL,
    [Phone] nvarchar(max)  NULL
);
GO

-- Creating table 'Customers'
CREATE TABLE [dbo].[Customers] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NULL,
    [Phone] nvarchar(20)  NULL,
    [email] nvarchar(max)  NOT NULL,
    [adress] nvarchar(max)  NULL,
    [Zipcode] nvarchar(max)  NULL,
    [City] nvarchar(max)  NULL,
    [Password] nvarchar(max)  NULL
);
GO

-- Creating table 'Employees'
CREATE TABLE [dbo].[Employees] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NULL,
    [EmployeesFunction] nvarchar(max)  NULL,
    [Phone] nvarchar(20)  NULL,
    [Email] nvarchar(max)  NOT NULL,
    [Password] nvarchar(max)  NULL,
    [CinemasId] int  NOT NULL
);
GO

-- Creating table 'Genres'
CREATE TABLE [dbo].[Genres] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Movies'
CREATE TABLE [dbo].[Movies] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [ReleaseDate] datetime  NULL,
    [AgeRating] int  NULL,
    [Plot] nvarchar(max)  NULL,
    [Info] nvarchar(max)  NULL,
    [TrailerUrl] nvarchar(max)  NULL,
    [FromDate] datetime  NULL,
    [ToDate] datetime  NULL,
    [Duration] int  NULL,
    [MovieType] nvarchar(max)  NULL,
    [ImageData] nvarchar(max)  NULL,
    [ImageMimeType] nvarchar(max)  NULL,
    [GenresId] int  NOT NULL,
    [MovieTypesId] int  NOT NULL
);
GO

-- Creating table 'MoviesShowings'
CREATE TABLE [dbo].[MoviesShowings] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [DateTime] datetime  NOT NULL,
    [MoviesId] int  NOT NULL,
    [RoomsId] int  NOT NULL,
    [RoomsCinemasId] int  NOT NULL
);
GO

-- Creating table 'MovieTypes'
CREATE TABLE [dbo].[MovieTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Type] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Rooms'
CREATE TABLE [dbo].[Rooms] (
    [Id] int  NOT NULL,
    [CinemasId] int  NOT NULL,
    [RoomNumber] int  NOT NULL
);
GO

-- Creating table 'Seats'
CREATE TABLE [dbo].[Seats] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [RowNumber] int  NOT NULL,
    [SeatNumber] int  NOT NULL,
    [RoomsId] int  NOT NULL,
    [RoomsCinemasId] int  NOT NULL
);
GO

-- Creating table 'SeatsReserveds'
CREATE TABLE [dbo].[SeatsReserveds] (
    [Id] int  NOT NULL,
    [SeatId] int  NOT NULL,
    [BookingsId] int  NOT NULL,
    [BookingTypesId] int  NOT NULL,
    [BookingOptionsId] int  NOT NULL,
    [Bookings_Id] int  NOT NULL
);
GO

-- Creating table 'BookingOptionsSetBookings'
CREATE TABLE [dbo].[BookingOptionsSetBookings] (
    [BookingOptionsSet_Id] int  NOT NULL,
    [Bookings_Id] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'BookingOptionsSet'
ALTER TABLE [dbo].[BookingOptionsSet]
ADD CONSTRAINT [PK_BookingOptionsSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Bookings'
ALTER TABLE [dbo].[Bookings]
ADD CONSTRAINT [PK_Bookings]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'BookingTypes'
ALTER TABLE [dbo].[BookingTypes]
ADD CONSTRAINT [PK_BookingTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Cinemas'
ALTER TABLE [dbo].[Cinemas]
ADD CONSTRAINT [PK_Cinemas]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Customers'
ALTER TABLE [dbo].[Customers]
ADD CONSTRAINT [PK_Customers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Employees'
ALTER TABLE [dbo].[Employees]
ADD CONSTRAINT [PK_Employees]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Genres'
ALTER TABLE [dbo].[Genres]
ADD CONSTRAINT [PK_Genres]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Movies'
ALTER TABLE [dbo].[Movies]
ADD CONSTRAINT [PK_Movies]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'MoviesShowings'
ALTER TABLE [dbo].[MoviesShowings]
ADD CONSTRAINT [PK_MoviesShowings]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'MovieTypes'
ALTER TABLE [dbo].[MovieTypes]
ADD CONSTRAINT [PK_MovieTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Rooms'
ALTER TABLE [dbo].[Rooms]
ADD CONSTRAINT [PK_Rooms]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Seats'
ALTER TABLE [dbo].[Seats]
ADD CONSTRAINT [PK_Seats]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id], [SeatId], [BookingsId] in table 'SeatsReserveds'
ALTER TABLE [dbo].[SeatsReserveds]
ADD CONSTRAINT [PK_SeatsReserveds]
    PRIMARY KEY CLUSTERED ([Id], [SeatId], [BookingsId] ASC);
GO

-- Creating primary key on [BookingOptionsSet_Id], [Bookings_Id] in table 'BookingOptionsSetBookings'
ALTER TABLE [dbo].[BookingOptionsSetBookings]
ADD CONSTRAINT [PK_BookingOptionsSetBookings]
    PRIMARY KEY CLUSTERED ([BookingOptionsSet_Id], [Bookings_Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [CustomersId] in table 'Bookings'
ALTER TABLE [dbo].[Bookings]
ADD CONSTRAINT [FK_FKBookings307328]
    FOREIGN KEY ([CustomersId])
    REFERENCES [dbo].[Customers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_FKBookings307328'
CREATE INDEX [IX_FK_FKBookings307328]
ON [dbo].[Bookings]
    ([CustomersId]);
GO

-- Creating foreign key on [MoviesShowingId] in table 'Bookings'
ALTER TABLE [dbo].[Bookings]
ADD CONSTRAINT [FK_FKBookings613582]
    FOREIGN KEY ([MoviesShowingId])
    REFERENCES [dbo].[MoviesShowings]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_FKBookings613582'
CREATE INDEX [IX_FK_FKBookings613582]
ON [dbo].[Bookings]
    ([MoviesShowingId]);
GO

-- Creating foreign key on [BookingTypesId] in table 'SeatsReserveds'
ALTER TABLE [dbo].[SeatsReserveds]
ADD CONSTRAINT [FK_FKSeatsReser457498]
    FOREIGN KEY ([BookingTypesId])
    REFERENCES [dbo].[BookingTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_FKSeatsReser457498'
CREATE INDEX [IX_FK_FKSeatsReser457498]
ON [dbo].[SeatsReserveds]
    ([BookingTypesId]);
GO

-- Creating foreign key on [CinemasId] in table 'Employees'
ALTER TABLE [dbo].[Employees]
ADD CONSTRAINT [FK_FKEmployees288785]
    FOREIGN KEY ([CinemasId])
    REFERENCES [dbo].[Cinemas]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_FKEmployees288785'
CREATE INDEX [IX_FK_FKEmployees288785]
ON [dbo].[Employees]
    ([CinemasId]);
GO

-- Creating foreign key on [CinemasId] in table 'Rooms'
ALTER TABLE [dbo].[Rooms]
ADD CONSTRAINT [FK_FKRooms613752]
    FOREIGN KEY ([CinemasId])
    REFERENCES [dbo].[Cinemas]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_FKRooms613752'
CREATE INDEX [IX_FK_FKRooms613752]
ON [dbo].[Rooms]
    ([CinemasId]);
GO

-- Creating foreign key on [GenresId] in table 'Movies'
ALTER TABLE [dbo].[Movies]
ADD CONSTRAINT [FK_FKMovies597736]
    FOREIGN KEY ([GenresId])
    REFERENCES [dbo].[Genres]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_FKMovies597736'
CREATE INDEX [IX_FK_FKMovies597736]
ON [dbo].[Movies]
    ([GenresId]);
GO

-- Creating foreign key on [MovieTypesId] in table 'Movies'
ALTER TABLE [dbo].[Movies]
ADD CONSTRAINT [FK_FKMovies768699]
    FOREIGN KEY ([MovieTypesId])
    REFERENCES [dbo].[MovieTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_FKMovies768699'
CREATE INDEX [IX_FK_FKMovies768699]
ON [dbo].[Movies]
    ([MovieTypesId]);
GO

-- Creating foreign key on [MoviesId] in table 'MoviesShowings'
ALTER TABLE [dbo].[MoviesShowings]
ADD CONSTRAINT [FK_FKMoviesShow559004]
    FOREIGN KEY ([MoviesId])
    REFERENCES [dbo].[Movies]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_FKMoviesShow559004'
CREATE INDEX [IX_FK_FKMoviesShow559004]
ON [dbo].[MoviesShowings]
    ([MoviesId]);
GO

-- Creating foreign key on [RoomsId] in table 'MoviesShowings'
ALTER TABLE [dbo].[MoviesShowings]
ADD CONSTRAINT [FK_FKMoviesShow710895]
    FOREIGN KEY ([RoomsId])
    REFERENCES [dbo].[Rooms]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_FKMoviesShow710895'
CREATE INDEX [IX_FK_FKMoviesShow710895]
ON [dbo].[MoviesShowings]
    ([RoomsId]);
GO

-- Creating foreign key on [RoomsId] in table 'Seats'
ALTER TABLE [dbo].[Seats]
ADD CONSTRAINT [FK_FKSeats497775]
    FOREIGN KEY ([RoomsId])
    REFERENCES [dbo].[Rooms]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_FKSeats497775'
CREATE INDEX [IX_FK_FKSeats497775]
ON [dbo].[Seats]
    ([RoomsId]);
GO

-- Creating foreign key on [SeatId] in table 'SeatsReserveds'
ALTER TABLE [dbo].[SeatsReserveds]
ADD CONSTRAINT [FK_FKSeatsReser829977]
    FOREIGN KEY ([SeatId])
    REFERENCES [dbo].[Seats]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_FKSeatsReser829977'
CREATE INDEX [IX_FK_FKSeatsReser829977]
ON [dbo].[SeatsReserveds]
    ([SeatId]);
GO

-- Creating foreign key on [Bookings_Id] in table 'SeatsReserveds'
ALTER TABLE [dbo].[SeatsReserveds]
ADD CONSTRAINT [FK_SeatsReservedsBookings]
    FOREIGN KEY ([Bookings_Id])
    REFERENCES [dbo].[Bookings]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SeatsReservedsBookings'
CREATE INDEX [IX_FK_SeatsReservedsBookings]
ON [dbo].[SeatsReserveds]
    ([Bookings_Id]);
GO

-- Creating foreign key on [BookingOptionsSet_Id] in table 'BookingOptionsSetBookings'
ALTER TABLE [dbo].[BookingOptionsSetBookings]
ADD CONSTRAINT [FK_BookingOptionsSetBookings_BookingOptionsSet]
    FOREIGN KEY ([BookingOptionsSet_Id])
    REFERENCES [dbo].[BookingOptionsSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Bookings_Id] in table 'BookingOptionsSetBookings'
ALTER TABLE [dbo].[BookingOptionsSetBookings]
ADD CONSTRAINT [FK_BookingOptionsSetBookings_Bookings]
    FOREIGN KEY ([Bookings_Id])
    REFERENCES [dbo].[Bookings]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_BookingOptionsSetBookings_Bookings'
CREATE INDEX [IX_FK_BookingOptionsSetBookings_Bookings]
ON [dbo].[BookingOptionsSetBookings]
    ([Bookings_Id]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------