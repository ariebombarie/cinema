﻿using Domain.Interfaces;
using Domain.Models;
using System;
using System.Linq;
using System.Web.Mvc;
using Website.Models;

namespace Website.Controllers
{
    public class SeatSelectionController : Controller
    {
        //Gemaakt door: Arie Visser
        //Getest door: Merel Huijben

        private readonly ISeatService _seats;
        public SeatSelectionController(ISeatService seatShowing)
        {
            _seats = seatShowing;
        }

        private cinemaEntities1 db = new cinemaEntities1();
        // GET: SeatSelection
        public ActionResult SeatSelection()
        {
            var m = _seats.GetSeats();

            var model = from seat in m
                        select new SeatModel
                        {
                            Id = seat.Id,
                            RowNumber = seat.RowNumber,
                            SeatNumber = seat.SeatNumber,
                            RoomsId = seat.RoomsId,
                            RoomsCinemasId = seat.RoomsCinemasId
                        };

            //Gemaakt door: Arie Visser
            //Getest door: Merel Huijben

            ViewBag.Title = "Bestelling afdrukken";
            ViewBag.Header = "Bestelling afdrukken";

            return View(model);

        }



    }
}
