﻿using Domain.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Website.Models
{

    public class MovieShowingsModel
    {
        public int Id { get; set; }
        public int urlId { get; set; }
        public int RoomNumber { get; set; }
        public int MoviesId { get; set; }
        public int MoviesIdFk { get; set; }
        public string Name { get; set; }
        public string ImageData { get; set; }
        public int GenresId { get; set; }
        public Nullable<int> AgeRating { get; set; }
        public string Plot { get; set; }
        public Nullable<int> Duration { get; set; }
        public string Trailer { get; set; }
        public int MovieTypeId { get; set; }
        public int MovieShowingId { get; set; }
        public string DateSelected { get; set; }
        public virtual MovieTypes MovieTypes { get; set; }
        public virtual Genres Genres { get; set; }
        public DateTime DateTime { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Bookings> Bookings { get; set; }
        public virtual Movies Movies { get; set; }
        public virtual Rooms Rooms { get; set; }
        public string Info { get; internal set; }
    }
}
