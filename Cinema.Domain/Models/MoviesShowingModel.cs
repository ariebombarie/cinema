﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Partials
{

        public class IndexModel
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string ImageData { get; set; }
            public int GenresId { get; set; }
            public Nullable<int> AgeRating { get; set; }
            public string Plot { get; set; }
            public Nullable<int> Duration { get; set; }
            public string Trailer { get; set; }
            public virtual Genres Genres { get; set; }
        }
    }
