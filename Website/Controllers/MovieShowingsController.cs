﻿using System.Linq;
using System.Web.Mvc;
using Domain.Models;
using Website.Models;
using Domain.Interfaces;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System;

namespace Website.Controllers
{
    public class MovieShowingsController : Controller
    {
        //Gemaakt door: Arie Visser
        //Getest door: Merel Huijben

        private readonly IMovieShowingsService _movies;
        public MovieShowingsController(IMovieShowingsService moviesShowing)
          {
            _movies = moviesShowing;
        }

    // GET: MovieShowings

    public ActionResult Movies(string Date)
        {

            var m = _movies.GetMovieShowings();

            if (Date == null){ Date = DateTime.Today.ToString("dd-MM-yyyy"); };

            var model = from movie in m
                        select new MovieShowingsModel
                        {
                            Id = movie.Id,
                            MoviesId = movie.Movies.Id,
                            MoviesIdFk = movie.MoviesId,
                            Info = movie.Movies.Info,
                            ImageData = movie.Movies.ImageData,
                            Name = movie.Movies.Name,
                            Genres = movie.Movies.Genres,
                            Plot = movie.Movies.Plot,
                            Duration = movie.Movies.Duration,
                            AgeRating = movie.Movies.AgeRating,
                            DateTime = movie.DateTime,
                            MovieTypes = movie.Movies.MovieTypes,
                            DateSelected = Date,
                            RoomNumber = movie.Rooms.RoomNumber
        };
            var DateSelected = Date;

            return View(model);

        }

        public ActionResult MoviesSelected(string Date, int id)
        {

            var m = _movies.GetMovieShowings();
            Session["UrlId"] = id;
            Session["UrlDate"] = Date;
            
            var id2 = Convert.ToInt32(Session["UrlId"]);


            if (Date == null) { Date = DateTime.Today.ToString("dd-MM-yyyy"); };

            var model = from movie in m
                        select new MovieShowingsModel
                        {
                            Id = movie.Id,
                            MoviesId = movie.Movies.Id,
                            ImageData = movie.Movies.ImageData,
                            Name = movie.Movies.Name,
                            Info = movie.Movies.Info,
                            Genres = movie.Movies.Genres,
                            Plot = movie.Movies.Plot,
                            Duration = movie.Movies.Duration,
                            AgeRating = movie.Movies.AgeRating,
                            DateTime = movie.DateTime,
                            MovieTypes = movie.Movies.MovieTypes,
                           // urlId = id,
                            DateSelected = Date,
                            RoomNumber = movie.Rooms.RoomNumber
                        };

            return View(model);

        }
    }
}