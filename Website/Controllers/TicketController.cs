﻿using System.Linq;
using System.Web.Mvc;
using Domain.Models;
using System.Data.Entity;
using Website.Models;


namespace Website.Controllers

{
    public class TicketController : Controller
    {
        //Gemaakt door: Arie Visser
        //Getest door: 
        private cinemaEntities1 db = new cinemaEntities1();

        // GET: Tickets
        public ActionResult Tickets(Movies model, MoviesShowings modelShowings)
        {
            Session["Moviename"] = model.Name;
            Session["MovieShowingsID"] = modelShowings.Id;

            var movieShowing = db.MoviesShowings.First(m => m.Id == model.Id);
            var allSeatIds = movieShowing.Rooms.Seats.Select(s => s.Id);
            var reservedSeatIds = movieShowing.Bookings.SelectMany(s => s.SeatsReserveds).Select(s => s.SeatId);
            var availableSeats = allSeatIds.Where(s => !reservedSeatIds.Contains(s)).Count();

            Session["AvailableSeats"] = availableSeats;

            ViewBag.Title = "Kies uw ticket(s)";
            ViewBag.Header = "Kies hier je ticket(s) voor: " + Session["Moviename"];

            var tickets = db.BookingTypes.Include(i => i.SeatsReserveds);
            var ticketOption = db.BookingOptionsSets.ToList();

            var movies = db.Movies.First(m => m.Name == model.Name);
            var movieType = movies.MovieTypes.Type;
            if (movieType == "3D")
            {
                TempData["3D"] = movieType;
            }
            else { TempData["3D"] = "Geen 3D!"; }

            var view = new TicketModel()
            {
                BookingTypes = tickets,
                BookingOptionsSets = ticketOption
            };


            return View(view);
        }
        public ActionResult TicketsSelected(int[] ticketKind, int[] totalTickets, int[] ticketOption, int[] totalOptionTickets)
        {

            //Gemaakt door: Arie Visser
            //Getest door: Merel Huijben


            //Gemaakt door: Arie Visser
            //Getest door: Merel Huijben

            var model = new Models.TicketsSelectedModel();
            var total = 0m; // Met d geef je aan dat het om een double gaat en met m een decimal (m staat voor money);
            var totalOptions = 0m;

            for (int i = 0; i < ticketKind.Count(); i++)
            {
                var ticketId = ticketKind[i];
                var bookingType = db.BookingTypes.First(b => b.Id == ticketId);
                var totalOrderedTickets = totalTickets[i];

                if (totalOrderedTickets > 0)
                {
                    model.ChosenTickets.Add(new ChosenTickets
                    {
                        TicketName = bookingType.Name,
                        TotalPrice = bookingType.Price.GetValueOrDefault(0) * totalOrderedTickets,
                        TotalOrdered = totalOrderedTickets,
                        TicketTypeId = ticketId,
                    });

                    total += bookingType.Price.GetValueOrDefault(0) * totalTickets[i];
                }
            }
            for (int o = 0; o < ticketOption.Count(); o++)
            {
                var ticketOptionId = ticketOption[o];
                var bookingOption = db.BookingOptionsSets.First(b => b.Id == ticketOptionId);
                var totalOrderedOptions = totalOptionTickets[o];

                if (totalOrderedOptions > 0)
                {
                    model.ChosenTickets.Add(new ChosenTickets
                    {
                        TicketOptionName = bookingOption.Name,
                        TotalOptionPrice = bookingOption.price * totalOrderedOptions,
                        TotalOptionOrdered = totalOrderedOptions,
                        TicketOptionTypeId = ticketOptionId,

                    });
                    totalOptions += bookingOption.price * totalOptionTickets[o];
                }
            }
            model.TotalPrice = total;
            model.TotalOptionsPrice = totalOptions;
            model.FinalPrice = total + totalOptions;

            ViewBag.Title = "Controle";
            ViewBag.Header = "Controleer uw bestelling!";
            Session["ChosenTickets"] = model;
            Session["TotalPrice"] = model.FinalPrice;

            return View(model);
        }
    }
}