﻿using Domain.Interfaces;
using Domain.Models;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Services
{
    public class SeatsDBService : ISeatService
    {
        private readonly cinemaEntities1 _context;

        public SeatsDBService()
        {
            _context = new cinemaEntities1();
        }

        public void Add(Seats seats)
        {
            _context.Seats.Add(seats);
        }

        public List<Seats> GetSeats()
        {
            return _context.Seats.ToList();
        }

        public List<Rooms> GetRooms()
        {
            var Rooms = new List<Rooms>();
            return Rooms;

        }
    }
}
