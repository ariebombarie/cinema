﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Cinema.Unittests.Mocks;
using Website.Controllers;
using System;


namespace Website.Controllers.Tests
{
    [TestClass()]
    public class TicketTypeTest
    {

        [TestMethod()]
        public void Ticket_Type_Test()
        {
            var service = new TicketsSelectedMock();
            var controller = new HomeController(service);


            // When
            controller.IndexBookingType();
            // then
               Assert.AreEqual(0, service.TicketTypeTest);
        }
}
}
