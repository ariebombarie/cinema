﻿using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Domain.Models;
using System;
using Cinema.WebUI.Models;
using System.Net;

namespace Cinema.Domain.Controllers
{
    public class ReservationController : Controller
    {
        private cinemaEntities1 db = new cinemaEntities1();

        // GET: Reservation
        public ActionResult Print(Guid? UniqueId)
        {
            if(UniqueId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bookings bookings = db.Bookings.Find(UniqueId);
            if (bookings == null)
            {
                return HttpNotFound();
            }
            return View(bookings);

        }
    }
}