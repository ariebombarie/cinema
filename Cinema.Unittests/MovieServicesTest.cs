﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Cinema.Unittests.Mocks;
using Website.Controllers;

namespace Cinema.Unittests
{
    [TestClass]
    public class MovieServicesTest
    {
        [TestMethod]
        public void Should_Return_List_Of_Showing_Movies()
        {
            // Given
            var service = new MoviesServiceMock();
            var controller = new HomeController(service);


            // When
            controller.Index();

            // Then
            Assert.AreEqual(1, service.GetMoviesShowingsCallCount);
        }
    }
}
